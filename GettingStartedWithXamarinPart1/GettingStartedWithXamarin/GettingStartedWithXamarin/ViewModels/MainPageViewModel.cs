﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GettingStartedWithXamarin.ViewModels
{
    public class MainPageViewModel : BindableBase, INavigationAware
    {
        private string _numberA;
        private string _numberB;
        private string _result;

        private string _title;

        public string NumberA
        {
            get { return _numberA; }
            set {
                SetProperty(ref _numberA, value);
                Sum();
            }
        }
        public string NumberB
        {
            get { return _numberB; }
            set
            {
                SetProperty(ref _numberB, value);
                Sum();
            }
        }
        public string Result
        {
            get { return _result; }
            set { SetProperty(ref _result, value); }
        }

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public MainPageViewModel()
        {

        }

        private void Sum()
        {
            try
            {
                var a = int.Parse(_numberA);
                var b = int.Parse(_numberB);
                Result = $"{a} + {b} = {a + b}";
            }
            catch
            {
                Result = "Invalid Input.";
            }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("title"))
                Title = (string)parameters["title"] + " and Prism";
        }
    }
}
